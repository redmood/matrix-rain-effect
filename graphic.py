import pygame
import stream

WINDOW_HEIGHT = 900
WINDOW_WIDTH = round(1.99*WINDOW_HEIGHT)

FONT_SIZE = 20

RED = (255,0,0)
RED_GLOW = (255,150,150)
BLUE = (0,0,255)
BLUE_GLOW = (150,150,255)

"""Choose the first available font within a given list of fonts. If none is
available, the font is set to the default system font. Set the size of the
font, to the size given in parameter."""
def chooseFont(fonts, size):
    #gets the list of fonts available in the system
    availableFonts = pygame.font.get_fonts()
    #in pygame, font names must have no spaces and no upper cases
    #There are two ways to create a list based on another
    #We can use generators and a for loop, to iterate through the first
    #list, while building the generator.
    choices = (fonts[i].lower().replace(' ', '') for i in range(0,len(fonts)))

    #We can use the map function coupled with lambda calculus
    #To be honest, I understand the notion of lambda calculus, since I already
    #met it in Java 8. But I am not familiar with it, yet. Especially in
    #Python. Will need to have a closer look at it (looks very cool !).
    #choices = map(lambda x:x.lower().replace(' ', ''), fonts)

    #Iterating over choices and checking if available
    for choice in choices:
        if choice in availableFonts:
            #if available, we can return the font and stop iteration
            return pygame.font.SysFont(choice, size)
        #If no choice were available, we return the default system font
        return pygame.font.Font(None, size)

"""Draw a symbol, assuming a defined font"""
def drawSymbol(surface,symbol,font):
    #coordinates of the center of the window
    x_c = round(WINDOW_WIDTH/2)
    y_c = round(WINDOW_HEIGHT/2)

    #used to control separation line inclination
    slide = -250

    #computing position of the symbol regarding separation line
    d = y_c*symbol.x - slide*symbol.y - y_c*x_c + slide*y_c

    #print("x, y, d =", symbol.x, symbol.y, d)

    color = BLUE
    if d < 0:
        #print("d negative")
        color = RED
        if symbol.glow:
            color = RED_GLOW
    else:
        if symbol.glow:
            color = BLUE_GLOW

    #Make symbol glow (or not, to draw the question mark)
    #Mark dot
    #center coordinates of the dot
    x_c = round(WINDOW_WIDTH/2)
    y_c = WINDOW_HEIGHT - 6*FONT_SIZE

    #draw lower point of the dot
    #if (symbol.x - x_c)**2 + (symbol.y - y_c)**2 <= FONT_SIZE**2:
    #    print("in")
    #    if color == RED:
    #        color = RED_GLOW
    #    elif color == BLUE:
    #        color = BLUE_GLOW
    #draw vertical line of dot
    #elif symbol.y < y_c - 3*FONT_SIZE and symbol.y > y_c - 13*FONT_SIZE:
    #    if symbol.x > x_c - FONT_SIZE and symbol.x < x_c + FONT_SIZE:
    #        if color == RED:
    #            color = RED_GLOW
    #        elif color == BLUE:
    #            color = BLUE_GLOW

    text = font.render(symbol.character,True,color)
    surface.blit(text,(symbol.x,symbol.y))

"""Make a symbol glow, in order to draw a interrogation mark on screen"""
def drawMark(symbol):

    #Mark dot
    #center coordinates of the dot
    x_c = round(WINDOW_WIDTH/2)
    y_c = WINDOW_HEIGHT - 10*FONT_SIZE

    if (symbol.x - x_c)**2 + (symbol.y - y_c)**2 <= 40**2:
        print("in")
        if color == RED:
            return RED_GLOW
        elif color == BLUE:
            return BLUE_GLOW



def drawStream(surface,stream,font):
    """Draws a stream of symbol, with its inner list of symbols"""
    for symbol in stream.symbols:
        drawSymbol(surface,symbol,font)

def drawStreams(surface,streams,font):
    """Draws a set of streams"""
    for stream in streams.streams:
        drawStream(surface,stream,font)
