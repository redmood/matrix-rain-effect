from random import randint

class Symbol:
    """docstring for Symbol. This class represent one Symbol. A Symbol has an
    initial position (x, y), given in parameter of the constructor."""
    def __init__(self, x, y, glow):
        self.x = x
        self.y = y
        self.glow = glow #some characters are a bit more glowing than the rest
        self.switchInterval = randint(4,20) #character change at random rate
        self.setToRandomCharacter(0)

    """Give a random character to a Symbol. Characters are chosen among the
    Latin alphabet in the ASCII table."""
    def setToRandomCharacter(self,count):
        if count % self.switchInterval == 0:#the character change only if it
            #reached his limit time
            #character is set from its keycode from ASCII table
            self.character = chr(65 + randint(0, 25))

class Stream():
    """docstring for Stream. Stream is a chain of Symbol, represented as an
    array of Symbol. The Stream builds itself a list of symbols, with
    random Characters. The size of the Stream is given as a parameter, in
    the Stream’s constructor."""
    def __init__(self,size,font_size,x,y,backward):
        self.symbols = []
        self.speed = randint(8,20) #the moving speed of the stream
        if backward:#if backward is true, the stream goes up, instead of down
            self.speed = -self.speed
        self.callCount = 0 #counts the number of time, the stream has been
        #updated. It were thought to keep track of the living time of the stream
        #allow symbol to modify themselves, according their changing rate
        self.x = x #the x coordinate of the stream. Since streams move
        #vertically, it stays constant and every symbol in the stream have the
        #same x.

        glow = randint(0,2) == 0 #first character of the stream glows, but not
        #always. Only a few streams have this glowing character.
        for i in range(0,size):
            self.symbols.append(Symbol(x,y-i*font_size,glow))
            glow = False

    def  update(self, height):
        """Updates position of each symbol, based on its speed value"""
        self.callCount += 1 #callCount is incremented at each update
        for symbol in self.symbols:
            symbol.y = (symbol.y + self.speed) % height
            #symbol changed if it is time for it to do so
            symbol.setToRandomCharacter(self.callCount)

class Streams():
    """An array of streams a list of streams."""
    def __init__(self,size,font_size,max_height):
        self.streams = []

        for i in range(0,size):
            #y init is randomized to avoid having all the streams starting at
            #the same y, at initialization
            yinit = randint(0,max_height)
            self.streams.append(Stream(randint(10,35),font_size,i*font_size,yinit,i%2==0))

    """Updates all the streams"""
    def update(self,height):
        for stream in self.streams:
            stream.update(height)

if __name__ == "__main__":
    #TEST OF Symbol
    symbol = Symbol(0,0)

    if symbol.x != 0:
        print("error symbol initializing x")

    if symbol.y != 0:
        print("error symbol initializing y")

    print(symbol.character)

    #TEST OF Stream
    size = 5
    stream = Stream(size)

    if len(stream.symbols) != size:
        print("error size stream: ", len(stream.symbols), size)
