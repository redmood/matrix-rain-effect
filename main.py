import stream
import graphic
import pygame
from pygame.locals import *

pygame.init()
fenetre = pygame.display.set_mode((graphic.WINDOW_WIDTH,graphic.WINDOW_HEIGHT))
clock = pygame.time.Clock()

fonts = ["Comic Sans MS", "Arial"]
font = graphic.chooseFont(fonts,graphic.FONT_SIZE)

streams = stream.Streams(round(graphic.WINDOW_WIDTH/graphic.FONT_SIZE),graphic.FONT_SIZE,graphic.WINDOW_HEIGHT)
graphic.drawStreams(fenetre,streams,font)

s = pygame.Surface((graphic.WINDOW_WIDTH,graphic.WINDOW_HEIGHT), pygame.SRCALPHA)
s.fill((0,0,0,130))
fenetre.blit(s, (0,0))
pygame.display.flip()

quit = False

while not quit:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit = True

    clock.tick(60)

    streams.update(graphic.WINDOW_HEIGHT)

    fenetre.blit(s, (0,0))
    graphic.drawStreams(fenetre,streams,font)

    pygame.display.flip()
