Matrix character rain effect
=

Recreates the Matrix character rain effect. The project is largely inspired from
a video of Emily Xie, on The Coding Train Youtube chanel:
https://www.youtube.com/watch?v=S1TQCi9axzg

The project is originally coded in Javascript, with the p5.js library. Wich is
really great for drawing in a browser. Since I am more of a Python developer, I
decided to do my own version in Python, using the pygame graphics library.

In terms of "innovation":

* Every two stream goes up, instead of down.

* I made the characters fall in red on the right side and blue on the left side, with a right-inclined separation line, in the middle of the screen. The characters that pass the frontier change their color.

This work is distributed under the creative commons [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) license.
